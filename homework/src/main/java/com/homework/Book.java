package com.homework;

import java.util.Arrays;

// 5. (Advanced): Book and Author Classes Again - An Array of Objects as an Instance Variable​
/**
 * In the earlier exercise, a book is written by one and only one author. In
 * reality, a book can be written by one or more author. Modify the Book class
 * to support one or more authors by changing the instance variable authors to
 * an Author array.​
 */
public class Book {
    private String name;
    private Author[] authors;
    private double price;
    private int qty = 0;

    public Book(String name, Author[] authors, double price) {
        this.name = name;
        this.authors = authors;
        this.price = price;
    }

    public Book(String name, Author[] authors, double price, int qty) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qty = qty;
    }

    public String getName() {
        return name;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    @Override
    public String toString() {
        return "Book [authors=" + Arrays.toString(authors) + ", name=" + name + ", price=" + price + ", qty=" + qty
                + "]";
    }

    public String getAuthorName() {
        return "authorsName1, authorsName2";

    }

}
