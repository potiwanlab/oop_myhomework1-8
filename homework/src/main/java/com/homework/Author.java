package com.homework;

// 4.The Author and Book Classes (An Introduction to OOP Composition)​
/**
 * This first exercise shall lead you through all the concepts involved in OOP
 * Composition.​
 * 
 * A class called Author (as shown in the class diagram) is designed to model a
 * book's author.
 */
public class Author {
    private String name;
    private String email;
    private char gender;

    public Author(String name, String email, char gender) {
        this.name = name;
        this.email = email;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public char getGender() {
        return gender;
    }

    @Override
    public String toString() {
        return "Author [email=" + email + ", gender=" + gender + ", name=" + name + "]";
    }
}
