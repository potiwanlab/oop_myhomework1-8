package com.homework.homework1_3;

public interface IWebsiteCreator {
    public void createWebsite(String template, String titleName);

}
