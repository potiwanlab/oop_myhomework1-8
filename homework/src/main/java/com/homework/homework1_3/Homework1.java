package com.homework.homework1_3;

public class Homework1 {
    public static void main(String[] args) {
        CEO marvel = new CEO("Captain", "Marvel", 30000);
        Employee somsri = new Employee("Somsri", "Sudsuay", 22000);
        OfficeCleaner somying = new OfficeCleaner(123, "somying", "yindee", 20000, "cleaner");
        somsri.gossip(marvel, "Today is very cold!"); // จงหาที่สร้าง method gossip เพื่อให้ code ตรงนี้เอา comment
                                                      // ออกแล้วสามารถทำงานได้
        marvel.gossip(somsri, "Today is very cold!"); // จงหาที่สร้าง method gossip เพื่อให้ code ตรงนี้เอา comment
        marvel.assignNewSalary(somsri, 20);
        marvel.assignNewSalary(somsri, 25000); // ออกแล้วสามารถทำงานได้
        marvel.work(somsri);

        // ให้สั่งให้ OfficeCleaner ให้ฆ่าแมลงสาบ 10
        marvel.orderKillCoachroach(somying);
        marvel.orderKillCoachroach(somying);
        System.out.println(somying.getTotalKilled());

    }
}
