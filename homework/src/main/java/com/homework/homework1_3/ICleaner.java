package com.homework.homework1_3;

import java.util.ArrayList;
import java.util.HashMap;

public interface ICleaner {
    public void cleaner();

    public int number();

    public ArrayList<HashMap<String, String>> getCleanedRoom();
}
