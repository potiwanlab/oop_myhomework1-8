package com.homework.homework1_3;

import java.util.ArrayList;
import java.util.HashMap;

public class AI implements ICleaner, ICoachroachKiller, IWebsiteCreator {
    private int totalKillCoachroach;

    public String name;
    public String language;

    public AI(String nameInput, String languageInput) {
        this.name = nameInput;
        this.language = languageInput;
    }

    @Override
    public void coachroachKiller() {
        totalKillCoachroach += 100;
        System.out.println("AI coachroachKiller");
    }

    @Override
    public void cleaner() {
        System.out.println("AIcleaner");
    }

    @Override
    public int number() {
        return 0;
    }

    @Override
    public ArrayList<HashMap<String, String>> getCleanedRoom() {
        return null;
    }

    @Override
    public int getTotalKilled() {
        return totalKillCoachroach;
    }

    public void clean() {
        System.out.println("AI clean");
    }

    @Override
    public void createWebsite(String template, String titleName) {
        // TODO Auto-generated method stub
        System.out.println(language + " automated Setup template: " + template);
        System.out.println(language + " automated Set Title name: " + titleName);

    }

}
