package com.homework.homework1_3;

import java.util.ArrayList;
import java.util.HashMap;

public class OfficeCleaner extends Employee implements ICleaner, ICoachroachKiller {
    private int totalKillCoachroach;

    ArrayList<HashMap<String, String>> cleanRoomDetail;

    public OfficeCleaner(int id, String firstnameInput, String lastnameInput, int salaryInput, String dressCode) {
        super(firstnameInput, lastnameInput, salaryInput);
        this.id = id;
        this.dressCode = dressCode;
        cleanRoomDetail = new ArrayList<>();
    }

    @Override
    public void work() {
        clean();
        killCoachroach();
        decorateRoom();
        welcomeGuest();
    }

    public void clean(String buildingName, String roomName) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(buildingName, roomName);
        cleanRoomDetail.add(hashMap);
        System.out.println("Office cleaner clean");
    }

    public void clean() {
        System.out.println("Office cleaner clean");
    }

    public void killCoachroach() {
        System.out.println("Office cleaner kill coachroach ");
    }

    public void decorateRoom() {
        System.out.println("Office cleaner decorate room");
    }

    public void welcomeGuest() {
        System.out.println("Office cleaner welcome guest");
    }

    @Override
    public void coachroachKiller() {
        totalKillCoachroach += 100;
        System.out.println(firstname + " coachroachKiller");
    }

    @Override
    public void cleaner() {
        System.out.println("cleaner");
    }

    @Override
    public int number() {
        return 1;
    }

    @Override
    public ArrayList<HashMap<String, String>> getCleanedRoom() {
        return cleanRoomDetail;
    }

    @Override
    public int getTotalKilled() {
        return totalKillCoachroach;
    }

}
