package com.homework.homework1_3;

import java.util.ArrayList;
import java.util.HashMap;

public class Homework2 {

    public static void main(String[] args) {
        String[] rawData = {
                "id:1001 firstname:Luke lastname:Skywalker salary:10000 type:frontend",
                "id:1002 firstname:Tony lastname:Stark salary:20000 type:backend",
                "id:1003 firstname:Somchai lastname:Jaidee salary:30000 type:fullstack",
                "id:1004 firstname:MonkeyD lastname:Luffee salary:40000 type:fullstack"
        };

        CEO faii = new CEO("Potiwan", "Lab-in", 300000);
        faii.initialEmployees(rawData);
        faii.printEmployees();

        /**
         * 2.ต่อจากการบ้านข้อ 1 จงเขียน Method getTotalKilled(); ใน class OfficeCleaner
         * และAI ให้ return ค่าจำนวนแมลงสาปที่ตายไปจริงๆ ออกมา โดยให้สมมติว่าการสั่ง
         * Method orderKillCoachroach() ใน class CEO หนึ่งครั้งสามารถฆ่าแมลงสาปได้จำนวน
         * 100 ตัว ไม่ว่าจะใช้ไบก้อนกี่กระป๋องก็ตาม หากเรียก killCoachroach()
         * สองครั้งจะฆ่าได้200 ตัว เป็นต้น (ดู Method orderWebsite เป็นตัวอย่าง
         * ให้ทำในลักษณะเดียวกัน)ส่วนค่า parameter ของ killCoachroach()
         * ให้ระบุอะไรก็ได้​
         */
        OfficeCleaner somying = new OfficeCleaner(123, "somying", "yindee", 20000, "cleaner");
        AI ai = new AI("alphaGo", "Java");

        faii.orderKillCoachroach(somying);
        faii.orderKillCoachroach(somying);
        System.out.println(somying.getTotalKilled());

        /**
         * จงเขียน Method orderCleaned() ใน class CEO ให้เรียกหา method clean() ใน class
         * OfficerCleaner และ AI ในลักษณะเดียวกับการ Method orderWebsite()
         * และให้แก้โครงสร้างของ getCleanedRoom() ใน ICleaner ให้เป็นดังนี้​
         * public ArrayList<HashMap<String,String>> getCleanedRoom();​
         */
        faii.orderCleaned(somying);
        faii.orderCleaned(ai);
        faii.orderCleaned(somying, "A", "101");
        ArrayList<HashMap<String, String>> roomDetail = somying.getCleanedRoom();
        for (HashMap<String, String> hashMap : roomDetail) {
            System.out.println(hashMap.toString());
        }
    }

}
