package com.homework;

/**
 * Hello world!
 */
public final class App {
    private App() {
    }

    /**
     * Says hello to the world.
     * 
     * @param args The arguments of the program.
     */
    public static void main(String[] args) {
        // point
        MyPoint p1 = new MyPoint();
        System.out.println(p1);
        p1.setXY(0, 3);
        System.out.println(p1);
        MyPoint p2 = new MyPoint(4, 0);
        System.out.println(p2);
        // Test distance() methods
        System.out.println(p1.distance(4, 0));
        System.out.println(p1.distance(p2));

        // circle
        MyCircle c1 = new MyCircle();
        System.out.println(c1);
        c1.setCenterXY(0, 3);
        System.out.println(c1);

        // Triangle
        MyTriangle t1 = new MyTriangle(1, 1, 3, 5, 6, 2);
        MyTriangle t2 = new MyTriangle(new MyPoint(1, 1), new MyPoint(3, 5), new MyPoint(6, 2));

        System.out.println("Triangle " + t1 + " is " + t1.getType() + " and has perimeter " + t1.getPerimeter());
        System.out.println("Triangle " + t2 + " is " + t2.getType() + " and has perimeter " + t2.getPerimeter());

    }
}
